Android Docker container
====

| Name |  Versions  |  Comments  |
|:--:|:--:|:--:|
| Java | 8 |  |
| Android SDK | 25 |  |
| Gradle | 3.2 |  |